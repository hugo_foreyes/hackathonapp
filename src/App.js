import React from "react";

import { Page, Tabbar, Tab, Navigator } from "react-onsenui";

import Home from "./pages/Home";

import { config as AmapReactConfig } from "@amap/amap-react";

AmapReactConfig.key = "4200893bd03df108e59d086534d93162";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animation: "slide-ios",
    };
  }

  renderPage(route, navigator) {
    route.props = route.props || {};
    route.props.navigator = navigator;

    return React.createElement(route.comp, route.props);
  }

  onPostPush(event) {
    if (this.state.animation != "slide-ios") {
      this.setState({
        animation: "slide-ios",
      });
    }
  }

  onPostPop(event) {}

  setAnimation(animation) {
    this.setState({
      animation: animation,
    });
  }

  render() {
    return (
      <Navigator
        ref={(instance) => {
          this.navigator = instance;
        }}
        initialRoute={{ comp: Home, props: { key: "home" } }}
        renderPage={this.renderPage}
        onPostPush={this.onPostPush.bind(this)}
        onPostPop={this.onPostPop}
        animation={this.state.animation}
        setAnimation={this.setAnimation.bind(this)}
      />
    );
  }
}

export default App;
