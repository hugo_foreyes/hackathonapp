import { useCallback, useState } from "react";
import { useMount, usePersistFn, useUnmount } from "ahooks";

const subscribers = {};
const globalValues = {};

/**
 * Use global state with hook.
 *
 * @param {string} key Unique name of state
 * @param {any} initValue Default value
 */
const useGlobalState = (key, initValue) => {
  const [value, setValue] = useState(globalValues[key] || initValue);

  const reRender = useCallback((val) => {
    setValue(val);
  }, []);

  useMount(() => {
    if (!(key in subscribers)) {
      subscribers[key] = [];
    }

    if (!(key in globalValues)) {
      globalValues[key] = initValue;
    }

    subscribers[key].push(reRender);
  });

  useUnmount(() => {
    subscribers[key] = subscribers[key].filter((s) => s !== reRender);
  });

  const updateState = usePersistFn((val) => {
    if (typeof val === "function") {
      globalValues[key] = val(globalValues[key]);
    } else {
      globalValues[key] = val;
    }

    const localSubscribers = subscribers[key];

    localSubscribers.forEach((local) => {
      local(val);
    });
  });

  return [value, updateState];
};

export default useGlobalState;
