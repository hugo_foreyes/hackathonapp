import React from 'react';
import LoadingGif from '../assets/img/loading_green.gif';

class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      	<div className={"loadingWp " + (!this.props.isLoading ? 'ldhidden' : 'ldvisible')}>
	      <div className='img-center-container height-100'>
	        <span></span>
	        <img className='loading' src={LoadingGif} alt="loading"/>
	      </div>
	    </div>
    );
  }
}

export default Loading
