import React from 'react';

import {
  Dialog
} from 'react-onsenui';
import WarningIcon from '../assets/img/warning.png';

class NoticePopup extends React.Component {
  constructor(props) {
    super(props);
  }

  closeDialog() {
    this.props.closeDialog();
  }

  render() {
    return (
      <Dialog className='dialog-confirm'
        isOpen={this.props.dialogOpen}
        onCancel={() => {this.closeDialog()}}
        cancelable>
        <div className='dl-wrapper'>
          <div className='dl-icon'>
            <span className="ion-ios-information-circle"></span>
          </div>
          <div className='dl-content'>
            {this.props.content}
          </div>
          <div className='dl-action tac'>
            <button className="bean-btn green" disabled={!this.props.dialogOpen} onClick={this.closeDialog.bind(this)}>Đã hiểu</button>
          </div>
        </div>
      </Dialog>
    );
  }
}

export default NoticePopup
