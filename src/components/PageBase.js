import React from 'react';
import {Page,Toolbar, Toast} from 'react-onsenui';
import Loading from './Loading';
import ConfirmPopup from './ConfirmPopup';
import NoticePopup from './NoticePopup';
import ReactDOM from "react-dom";
import {
  apiGet,
  apiPut,
  apiPostFiles,
  apiPost,
  apiDelete
} from '../helper/apiConnector';

class PageBase extends React.Component {

  constructor(props) {
    super(props);
    this.hasBackButton = true;
    this.className = '';
    this.state = {
      isLoading: false,
      showToast: false,
      textToast: "",
      confirmPopupIsOpen: false,
      confirmPopupContent: "",
      confirmPopupButton:"",
      noticePopupIsOpen: false,
      noticePopupContent: "",
    };
    this.resolveConfirmPopup = null;
    this.isHaveToolbar = true;
    this.onCloseNoticePopup = null;
  }

  changeBrowser(title, key) {
    document.title = title;
    window.history.pushState({}, title, '/#' + key);
  }

  loading(isLoading) {
    if (this.state.isLoading == isLoading) {
      return;
    }
    this.setState({
      isLoading: isLoading
    });
  }

  backToPreviousPage() {
    this.props.navigator.popPage();
    window.history.back();
  }

  goToNextPage(component, key) {
    this.props.navigator.pushPage({comp: component, props: { key }});
    this.changeBrowser(component.gaPage, key);
  }

  goToNextPageWithProps(component, key, props) {
    if (typeof props['animation'] !== 'undefined') {
      this.props.navigator.props.setAnimation(props['animation']);
    }
    props["key"] = key;
    this.props.navigator.pushPage({comp: component, props: props});
    this.changeBrowser(component.gaPage, key);
  }

  replacePage(component, key) {
    this.props.navigator.replacePage({comp: component, props: { key }});
    this.changeBrowser(component.gaPage, key);
  }

  replacePageWithProps(component, key, props) {
    if (typeof props['animation'] !== 'undefined') {
      this.props.navigator.props.setAnimation(props['animation']);
    }
    setTimeout(() => {
      props["key"] = key;
      this.props.navigator.replacePage({comp: component, props: props});
      this.changeBrowser(component.gaPage, key);
    });
  }

  resetPage(component, key, animationType) {
    if (typeof animationType === 'undefined') {
      animationType = 'lift';
    }
    this.props.navigator.props.setAnimation(animationType);
    setTimeout(() => {
      this.props.navigator.resetPage({comp: component, props: { key }});
      this.changeBrowser(component.gaPage, key);
    });
  }

  async handleOpenConfirmPopup(content, buttonLabel) {
    this.setState({
      confirmPopupIsOpen: true,
      confirmPopupContent: content,
      confirmPopupButton: buttonLabel
    });

    return new Promise(resolve => {
        this.resolveConfirmPopup = resolve;
    });
  }

  handleOpenNoticePopup(content, onClose) {
    this.setState({
      noticePopupIsOpen: true,
      noticePopupContent: content
    });
    if (typeof onClose !== 'undefined') {
      this.onCloseNoticePopup = onClose;
    } else {
      this.onCloseNoticePopup = null;
    }
  }

  closeConfirmPopup(isConfirm) {
    this.setState({
      confirmPopupIsOpen: false
    });
    this.resolveConfirmPopup(isConfirm);
  }

  closeNoticePopup() {
    this.setState({
      noticePopupIsOpen: false
    });
    if (null != this.onCloseNoticePopup) {
      this.onCloseNoticePopup();
    }
  }

  showToast(message) {
    this.setState({
      showToast: true,
      textToast: message
    });
  }

  autoCloseToast() {
    setTimeout(() => {
      this.setState({showToast: false});
    }, 1000);
  }

  onInfiniteScroll() {

  }

  processResponseApi(response, onSuccess, onFailed) {
    if (response.code == 200) {
      return onSuccess(response.data);
    }

    this.loading(false);
    if (onFailed) {
      onFailed(response.error);
    } else {
      this.handleOpenNoticePopup(response.error.message);
    }
  }

  renderToolbar() {
    if (!this.isHaveToolbar) {
      return null;
    }

    let backButton;
    if (this.hasBackButton) {
      backButton = <span onClick={this.backToPreviousPage.bind(this)} className='ion-ios-arrow-round-back toolbar-button back-button'>
          </span>;
    } else {
      backButton = <span></span>;
    }

    let action;
    if (this.renderToolbarAction) {
      action = this.renderToolbarAction();
    } else {
      action = <span></span>
    }

    return this.renderToolbarTemplate(backButton, action);
  }

  renderToolbarTemplate(backButton, action) {
    return (
      <Toolbar className="bean-toolbar">
        <div className='center'>
          {backButton}
          <div className='bean-title'>{this.pageName}</div>
          {action}
        </div>
      </Toolbar>
    );
  }

  render() {
    return (
      <Page renderToolbar={this.renderToolbar.bind(this)} className={'bean-page ' + this.className}
        onInfiniteScroll={this.onInfiniteScroll.bind(this)}
      >
        <Loading isLoading={this.state.isLoading} />
        {this.renderContent()}
        <Toast isOpen={this.state.showToast} onPostShow={this.autoCloseToast.bind(this)}>
          <span>{this.state.textToast}</span>
        </Toast>
        <ConfirmPopup dialogOpen={this.state.confirmPopupIsOpen}
          content={this.state.confirmPopupContent}
          actionConfirmLabel={this.state.confirmPopupButton}
          closeDialog={this.closeConfirmPopup.bind(this)}
        />
        <NoticePopup dialogOpen={this.state.noticePopupIsOpen}
          content={this.state.noticePopupContent}
          closeDialog={this.closeNoticePopup.bind(this)}
        />
      </Page>
    );
  }
}

export default PageBase
