import React from 'react';

import {
  Dialog
} from 'react-onsenui';
import WarningIcon from '../assets/img/warning.png';

class ConfirmPopup extends React.Component {
  constructor(props) {
    super(props);
  }

  closeDialog() {
    this.props.closeDialog(false);
  }

  confirm() {
    this.props.closeDialog(true);
  }

  render() {
    return (
      <Dialog className='dialog-confirm'
        isOpen={this.props.dialogOpen}
        onCancel={() => {this.closeDialog()}}
        cancelable>
        <div className='dl-wrapper'>
          <div className='dl-icon'>
            <img className='warning-icon-item' src={WarningIcon} alt="warning"/>
          </div> 
          <div className='dl-content'>
            {this.props.content}
          </div> 
          <div className='dl-action'>
            <button className="bean-btn red" disabled={!this.props.dialogOpen} onClick={() => this.closeDialog()}>Huỷ bỏ</button>
            <button className="bean-btn green fr" disabled={!this.props.dialogOpen} onClick={this.confirm.bind(this)}>{this.props.actionConfirmLabel}</button>
          </div>
        </div>
      </Dialog>
    );
  }
}

export default ConfirmPopup
