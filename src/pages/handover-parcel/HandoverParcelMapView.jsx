import React from "react";

import { Amap } from "@amap/amap-react";
import AMapMarker from "./AMapMarker";

const HandoverParcelMapView = ({
  destionations = [],
  backToPreviousPage,
  navigateToReturnHub,
  ...props
}) => {
  return (
    <div style={{ width: "100%", height: "350px", marginTop: "0px" }}>
      <Amap
        mapStyle="amap://styles/normal"
        zoom={15}
        lang="en"
        animateEnable
        vectorMapForeign="style_lazada"
        overseaDataType="mapbox"
      >
        <AMapMarker
          destionations={destionations}
          backToPreviousPage={backToPreviousPage}
          navigateToReturnHub={navigateToReturnHub}
        />
      </Amap>
    </div>
  );
};

export default HandoverParcelMapView;
