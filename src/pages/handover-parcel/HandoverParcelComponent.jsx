import React, { useCallback, useEffect, useState } from "react";
import { Input } from "react-onsenui";
import { BackButton } from "react-onsenui";
import { Button, Checkbox, Toolbar } from "react-onsenui";
import Loading from "../../components/Loading";
import { apiGet, apiPost } from "../../helper/apiConnector";
import { COURIER_LOCATION } from "../../constants";
import useGlobalState from "../../hooks/useGlobalState";
import HandoverParcelMapView from "./HandoverParcelMapView";

const mockData = [
  {
    id: 1234,
    name: "SSG",
    type: "Sortation",
    cutOffTime: "12:00",
    address: "12 Quang Trung, Quan Go Vap, TPHCM",
    phone: "0989788748",
    geoLat: 10.776956,
    geoLong: 106.683288,
    distance: 1.4, //in km
  },
  {
    id: 1235,
    name: "SSG-2",
    type: "Sortation",
    cutOffTime: "12:00",
    address: "12 Quang Trung, Quan Go Vap, TPHCM",
    phone: "0989788748",
    geoLat: 10.786956,
    geoLong: 106.683288,
    distance: 1.4, //in km
  },
];

const HandoverParcelComponent = ({
  processResponseApi,
  backToPreviousPage,
  navigateToReturnHub,
  ...props
}) => {
  const [fullDestinations, setFullDestinations] = useState([]);
  const [destionations, setDestionations] = useState([]);
  const [filterDestinations, setFilterDestinations] = useState([]);

  const [isLoading, setIsLoading] = useState(false);
  const [showMap, setShowMap] = useState(false);
  const [selectedDestionation, setSelectedDestination] = useState(null);

  const [courierLocation] = useGlobalState(COURIER_LOCATION, []);

  const loadFullData = useCallback(async () => {
    setIsLoading(true);
    try {
      const response = await apiGet("nodes/list");
      processResponseApi(response, (data) => {
        if (!data) {
          data = mockData;
        }
        setFullDestinations(data || []);
        setIsLoading(false);
      });
    } catch {
      setIsLoading(false);
      setFullDestinations(mockData);
    }
  }, [processResponseApi]);

  const loadData = useCallback(
    async (geoLat = 10.776956, geoLong = 106.683288) => {
      setIsLoading(true);
      try {
        const response = await apiPost("nodes/by-location", {
          geoLat,
          geoLong,
        });
        processResponseApi(response, (data) => {
          if (!data) {
            data = mockData;
          }
          setDestionations(data || []);
          setIsLoading(false);
          setFilterDestinations(data || []);
        });
      } catch {
        setIsLoading(false);
        setDestionations(mockData);
        setFilterDestinations(mockData);
      }
    },
    [processResponseApi]
  );

  const changeCourierStatus = useCallback(async () => {
    setIsLoading(true);
    try {
      await apiPost("couriers/change-status", {
        courierId: 1,
        status: "RETURNING",
      });
    } catch {}
    setIsLoading(false);
    navigateToReturnHub();
  }, [navigateToReturnHub]);

  useEffect(() => {
    loadData(...courierLocation);
  }, [loadData, courierLocation]);

  useEffect(() => {
    loadFullData();
  }, []);

  return (
    <div>
      <Toolbar className="bean-toolbar">
        <BackButton
          style={{ transform: "translateY(7px)" }}
          className="back-btn"
        />
        <div className="bean-title">
          <div className="select-destionation-page-header">
            Handover Parcel
            {!showMap ? (
              <span
                className="ion-md-navigate"
                onClick={() => {
                  setShowMap(!showMap);
                }}
              ></span>
            ) : (
              <span
                className="ion-md-list"
                onClick={() => {
                  setShowMap(!showMap);
                }}
              ></span>
            )}
          </div>
        </div>
      </Toolbar>

      <div className="select-destination-hub-container">
        <Loading isLoading={isLoading} />
        <div className="search-box">
          <div>Choose your destionation</div>
          <Input
            type="search"
            style={{
              // width: "90%",
              padding: "20px",
              border: "1px solid #999",
            }}
            className="search-input"
            onChange={(e) => {
              if (!e.target.value) {
                setFilterDestinations(destionations);
                return;
              }

              setFilterDestinations(
                fullDestinations.filter((s) =>
                  s.name?.toLowerCase().includes(e.target.value.toLowerCase())
                )
              );
            }}
          />
        </div>

        <div className="top-part">
          <span>Please select any hub to handover parcels</span>
        </div>

        {!showMap ? (
          <>
            <div className="destination-hub-body">
              {filterDestinations.map((destination, index) => {
                return (
                  <div
                    key={`destination-hub-item-${index}`}
                    className="hub-item-wrapper"
                  >
                    <div className="hub-item-header">
                      <div>{destination.name || "-"}</div>
                      <div className="hub-item-header-group">
                        <Checkbox
                          checked={selectedDestionation?.id === destination.id}
                          onChange={(e) => {
                            if (e.target.checked) {
                              setSelectedDestination(destination);
                              return;
                            }

                            selectedDestionation(null);
                          }}
                        />
                      </div>
                    </div>
                    <div className="hub-item-info">
                      {destination?.address || "-"}
                    </div>
                    <div className="hub-item-info">
                      {destination?.phone || "-"}
                    </div>

                    <div className="hub-item-bottom">
                      Distance: {destination?.distance || "0"} km
                    </div>
                  </div>
                );
              })}
            </div>

            <div className="destination-hub-actions">
              <Button
                disabled={!selectedDestionation}
                className="btn-large"
                onClick={changeCourierStatus}
              >
                Start
              </Button>
            </div>
          </>
        ) : (
          <HandoverParcelMapView
            destionations={filterDestinations}
            backToPreviousPage={backToPreviousPage}
            navigateToReturnHub={navigateToReturnHub}
          />
        )}
      </div>
    </div>
  );
};

export default HandoverParcelComponent;
