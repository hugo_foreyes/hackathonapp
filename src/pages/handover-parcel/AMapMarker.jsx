import React, { useCallback, useEffect, useState } from "react";
import { Button } from "react-onsenui";

import { useAmap } from "@amap/amap-react";
import useGlobalState from "../../hooks/useGlobalState";
import { SELECTED_DESTINATION } from "../../constants";
import { apiPost } from "../../helper/apiConnector";

const AMapMarker = ({
  destionations = [],
  backToPreviousPage,
  navigateToReturnHub,
  ...props
}) => {
  const aMap = useAmap();
  const [selectedDestination, setSelectedDestination] = useState(null);
  const [markers, setMarkers] = useState([]);
  const [, setSelectedDestinationGlobal] = useGlobalState(
    SELECTED_DESTINATION,
    {}
  );

  useEffect(() => {
    if (!aMap) return;

    aMap.remove(markers);

    const localMarkers = [];
    for (const destination of destionations) {
      const marker = new window.AMap.Marker({
        position: new window.AMap.LngLat(
          destination.geoLong,
          destination.geoLat
        ),
        label: {
          content: `${destination.name}`,
          direction: "bottom",
        },
        clickable: true,
        extData: {
          ...destination,
        },
      });

      marker.on("click", () => {
        setSelectedDestination(destination);
      });

      localMarkers.push(marker);
    }

    setMarkers(localMarkers);

    aMap.add(localMarkers);

    aMap.setFitView();
  }, [aMap, destionations]);

  useEffect(() => {
    if (!selectedDestination) return;

    aMap.remove(markers);

    let newMarkers = [];
    for (const destination of destionations) {
      const marker = new window.AMap.Marker({
        position: new window.AMap.LngLat(
          destination.geoLong,
          destination.geoLat
        ),
        label: {
          content: `${destination.name}`,
          direction: "bottom",
        },
        clickable: true,
      });
      if (destination.id === selectedDestination.id) {
        marker.setIcon(
          new window.AMap.Icon({
            image: "https://jimnox.gitee.io/amap-react/img/marker-1.svg",
          })
        );
      }
      marker.on("click", () => {
        if (destination.id === selectedDestination.id) {
          setSelectedDestination({
            id: "",
          });
          return;
        }
        setSelectedDestination(destination);
      });
      newMarkers.push(marker);
    }

    setMarkers(newMarkers);

    aMap.add(newMarkers);

    aMap.setFitView();
  }, [selectedDestination, aMap, destionations]);

  const changeCourierStatus = useCallback(async () => {
    try {
      await apiPost("couriers/change-status", {
        courierId: 1,
        status: "RETURNING",
      });
    } catch {}
    navigateToReturnHub();
  }, [navigateToReturnHub]);

  return (
    <div className="destination-hub-actions">
      <Button className="btn-large" onClick={changeCourierStatus}>
        Start
      </Button>
    </div>
  );
};

export default AMapMarker;
