import React from "react";
import PageBase from "../../components/PageBase";
import ReturnHub from "../ReturnHub";
import HandoverParcelComponent from "./HandoverParcelComponent";

import "./index.css";

class DestinationHub extends PageBase {
  static gaPage = "HandoverParcel";

  constructor(props) {
    super(props);
    this.state = {
      showMap: false,
    };
    this.isHaveToolbar = false;
  }

  renderContent() {
    return (
      <div>
        <HandoverParcelComponent
          processResponseApi={this.processResponseApi}
          backToPreviousPage={this.backToPreviousPage.bind(this)}
          navigateToReturnHub={this.resetPage.bind(
            this,
            ReturnHub,
            "return-hub"
          )}
        />
      </div>
    );
  }
}

export default DestinationHub;
