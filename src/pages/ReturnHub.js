import React from "react";
import PageBase from "../components/PageBase";
import ImgOnTheWay from "../assets/img/ontheway.png";
import { apiPost } from "../helper/apiConnector";
import FreePickup from "./free-pickup";

class ReturnHub extends PageBase {
  static gaPage = "ReturnHub";

  constructor(props) {
    super(props);
    this.pageName = "On the way to hub";
    this.state = {};
  }

  async finishHandover() {
    this.loading(true);
    let response = await apiPost("couriers/change-status", {
      courierId: 1,
      status: "FREE",
    });
    this.processResponseApi(response, (data) => {
      this.replacePage.bind(this, FreePickup, "free-pickup")();
      this.loading(false);
    });
  }

  renderContent() {
    return (
      <div className="page-content h-100 pickup-page">
        <div className="eptc">
          <img src={ImgOnTheWay} />
        </div>

        <div className="bottom-action">
          <button
            className="bean-btn green"
            onClick={this.finishHandover.bind(this)}
          >
            Finish Handover
          </button>
        </div>
      </div>
    );
  }
}

export default ReturnHub;
