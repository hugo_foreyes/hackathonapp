import React from "react";
import PageBase from "../components/PageBase";
import { Checkbox, Radio } from "react-onsenui";
import ImgBox from "../assets/img/box.png";
import FreePickup from "./free-pickup";

import { apiGet, apiPost } from "../helper/apiConnector";
import { COURIER_FREE } from "../constants";

class Pickup extends PageBase {
  static gaPage = "Pickup";

  constructor(props) {
    super(props);
    this.pageName = "Sellers To Pickup";
    this.hasBackButton = false;
    this.state = {
      sellers: [],
      selectedSeller: null,
    };
  }

  componentDidMount() {
    this.loadData();
  }

  async loadData() {
    this.loading(true);
    let response = await apiPost("sellers/by-courier-id", { courierId: 1 });
    this.processResponseApi(response, (data) => {
      this.setState({
        sellers: data,
      });
      this.loading(false);
    });
  }

  onSelectSeller(event, sellerId) {
    if (event.target.checked) {
      this.setState({
        selectedSeller: sellerId,
      });
    }
  }

  async changeCourierStatus(status = COURIER_FREE) {
    try {
      await apiPost("couriers/change-status", {
        courierId: 1,
        status,
      });

      this.replacePage.bind(this, FreePickup, "free-pickup")();
    } catch {}
  }

  async onFinish(isSkip) {
    if (this.state.selectedSeller == null) {
      return;
    }

    this.loading(true);
    let data = {
      courierId: 1,
      sellerId: this.state.selectedSeller,
      isFree: this.state.sellers.length < 2,
      isSkip: isSkip,
    };
    let response = await apiPost("couriers/finish-pick-up-at-stop", data);
    this.processResponseApi(response, (data) => {
      let finishSellerIndex = this.state.sellers.findIndex(
        (o) => o.id == this.state.selectedSeller
      );
      this.state.sellers.splice(finishSellerIndex, 1);
      if (!this.state.sellers.length) {
        this.changeCourierStatus();
      }
      this.setState({
        sellers: this.state.sellers,
        selectedSeller: null,
      });
      this.loading(false);
    });
  }

  componentDidUpdate() {}

  renderContent() {
    return (
      <div className="page-content pickup-page">
        {this.state.sellers.map((seller, id) => (
          <div className="seller-item" key={id}>
            <div className="header">
              <div className="name">{seller.name}</div>
              <div className="cb">
                <Checkbox
                  checked={
                    seller.id == this.state.selectedSeller ? true : false
                  }
                  onChange={(event) => {
                    this.onSelectSeller(event, seller.id);
                  }}
                />
              </div>
            </div>
            <div className="info">
              <div className="main">
                <div className="title">Address</div>
                <div className="text">{seller.address}</div>
                <div className="title">Phone</div>
                <div>{seller.phone}</div>
              </div>
              <div className="action">
                <div className="total_rts">
                  <img src={ImgBox} alt="box" />
                  <span> {seller.totalRts} </span>
                </div>
                <div className="call">
                  <button className="bean-btn green">
                    <span className="ion-ios-call"></span> Call
                  </button>
                </div>
              </div>
            </div>
          </div>
        ))}
        <div className="bottom-action">
          <button
            className="bean-btn green"
            onClick={this.onFinish.bind(this, false)}
          >
            Finish
          </button>
          <button
            className="bean-btn red"
            onClick={this.onFinish.bind(this, true)}
          >
            Skip
          </button>
        </div>
      </div>
    );
  }
}

export default Pickup;
