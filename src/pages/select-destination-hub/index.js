import React from "react";
import PageBase from "../../components/PageBase";
import SelectDestinationHubComponent from "./SelectDestinationHubComponent";

import "./index.css";

class DestinationHub extends PageBase {
  static gaPage = "SelectDestinationHub";

  constructor(props) {
    super(props);
    this.state = {
      showMap: false,
    };
    this.isHaveToolbar = false;
  }

  renderContent() {
    return (
      <div>
        <SelectDestinationHubComponent
          processResponseApi={this.processResponseApi}
          backToPreviousPage={this.backToPreviousPage.bind(this)}
        />
      </div>
    );
  }
}

export default DestinationHub;
