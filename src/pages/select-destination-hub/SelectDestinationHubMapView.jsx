import React from "react";

import { Amap } from "@amap/amap-react";
import AMapMarker from "./AMapMarker";

const SelectDestinationHubMapView = ({
  destionations = [],
  backToPreviousPage,
  ...props
}) => {
  return (
    <div style={{ width: "100%", height: "350px", marginTop: "10px" }}>
      <Amap
        mapStyle="amap://styles/normal"
        zoom={15}
        lang="en"
        animateEnable
        vectorMapForeign="style_lazada"
        overseaDataType="mapbox"
      >
        <AMapMarker
          destionations={destionations}
          backToPreviousPage={backToPreviousPage}
        />
      </Amap>
    </div>
  );
};

export default SelectDestinationHubMapView;
