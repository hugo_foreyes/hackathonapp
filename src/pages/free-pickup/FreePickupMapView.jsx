import React from "react";

import { Amap } from "@amap/amap-react";
import AMapMarker from "./AMapMarker";

const DestinationHubMapView = ({
  sellers = [],
  goToPickUpPage,
  goToHandoverParcel,
  ...props
}) => {
  return (
    <div style={{ width: "100%", height: "470px", marginTop: "110px" }}>
      <Amap
        mapStyle="'amap://styles/normal"
        animateEnable
        features={["bg", "road", "building", "point"]}
        vectorMapForeign="style_lazada"
        overseaDataType="mapbox"
      >
        <AMapMarker
          sellers={sellers}
          goToPickUpPage={goToPickUpPage}
          goToHandoverParcel={goToHandoverParcel}
        />
      </Amap>
    </div>
  );
};

export default DestinationHubMapView;
