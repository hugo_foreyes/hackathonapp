import React, { useCallback, useEffect, useState } from "react";
import { Button } from "react-onsenui";

import { useAmap } from "@amap/amap-react";
import { apiPost } from "../../helper/apiConnector";
import { COURIER_PICKING, COURIER_RETURNING } from "../../constants";

const AMapMarker = ({
  sellers = [],
  goToPickUpPage,
  goToHandoverParcel,
  ...props
}) => {
  const aMap = useAmap();
  const [selectedMarkers, setSelectedMarkers] = useState([]);

  useEffect(() => {
    if (!aMap || !sellers.length) return;

    const markers = [];
    for (const seller of sellers) {
      const marker = new window.AMap.Marker({
        position: new window.AMap.LngLat(seller.geoLong, seller.geoLat),
        label: {
          content: `RTS: ${seller.totalRts}, Distance: ${
            seller?.distance ? seller.distance.toFixed(2) : "-"
          } km`,
          direction: "bottom",
        },
        clickable: true,
      });

      marker.on("click", () => {
        const image = marker.getIcon()?._opts?.image;
        if (image !== "https://jimnox.gitee.io/amap-react/img/marker-1.svg") {
          marker.setIcon(
            new window.AMap.Icon({
              image: "https://jimnox.gitee.io/amap-react/img/marker-1.svg",
            })
          );
          setSelectedMarkers((prev) => [...prev, seller.id]);
          return;
        }

        setSelectedMarkers((prev) => prev.filter((s) => s !== seller.id));
        marker.setIcon(
          new window.AMap.Icon({
            image: "http://webapi.amap.com/theme/v1.3/markers/b/mark_bs.png",
            imageSize: new window.AMap.Size(19, 32),
          })
        );
      });

      markers.push(marker);
    }

    aMap.add(markers);

    aMap.setFitView();
  }, [aMap, sellers]);

  const setListSellersForCourier = useCallback(async (sellerIds = []) => {
    try {
      await apiPost("sellers/set-sellers-for-courier", {
        courierId: 1,
        sellerIds,
      });
    } catch {}
  }, []);

  const changeCourierStatus = useCallback(async (status = COURIER_PICKING) => {
    try {
      await apiPost("couriers/change-status", {
        courierId: 1,
        status,
      });
    } catch {}
  }, []);

  return (
    <div className="destination-hub-actions">
      <Button
        className="btn-large"
        disabled={!selectedMarkers.length}
        onClick={async () => {
          await changeCourierStatus();
          await setListSellersForCourier(
            sellers
              .filter((s) => selectedMarkers.includes(s.id))
              .map((s) => s.id)
          );
          goToPickUpPage(
            sellers.filter((s) => selectedMarkers.includes(s.id))
          )();
        }}
      >
        Start
      </Button>
      <Button
        className="btn-large"
        onClick={async () => {
          goToHandoverParcel();
        }}
      >
        Full Capacity
      </Button>
    </div>
  );
};

export default AMapMarker;
