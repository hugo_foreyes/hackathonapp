import React from "react";
import PageBase from "../../components/PageBase";
import Pickup from "../Pickup";
import FreePickupComponent from "./FreePickupComponent";
import SelectDestinationHubComponent from "../select-destination-hub";
import HandoverParcelComponent from "../handover-parcel";

import "./index.css";

class DestinationHub extends PageBase {
  static gaPage = "FreePickup";

  constructor(props) {
    super(props);
    this.state = {};
    this.isHaveToolbar = false;
  }

  componentDidMount() {}

  renderContent() {
    return (
      <div>
        <FreePickupComponent
          courierInfo={this.props.courierInfo}
          goToPickUpPage={(sellers) =>
            this.goToNextPageWithProps.bind(this, Pickup, "pickup", {
              sellers,
            })
          }
          goToSelectionHub={this.goToNextPage.bind(
            this,
            SelectDestinationHubComponent,
            "select-destination-hub"
          )}
          goToHandoverParcel={this.goToNextPage.bind(
            this,
            HandoverParcelComponent,
            "select-handover-parcel"
          )}
          processResponseApi={this.processResponseApi}
        />
      </div>
    );
  }
}

export default DestinationHub;
