import React, { useCallback, useEffect, useState } from "react";
import { Checkbox, Button, Toolbar, BackButton } from "react-onsenui";
import Loading from "../../components/Loading";

import { apiGet, apiPost } from "../../helper/apiConnector";
import useGlobalState from "../../hooks/useGlobalState";
import FreePickupMapView from "./FreePickupMapView";
import ImgBox from "../../assets/img/box.png";

import {
  COURIER_FREE,
  COURIER_LOCATION,
  COURIER_PICKING,
  COURIER_RETURNING,
  SELECTED_DESTINATION,
} from "../../constants";
import { Dialog, Input } from "react-onsenui";

const mockData = [
  {
    id: 12,
    name: "Shop ABC",
    phone: "0989788748",
    address: "35 Nguyễn Văn Thủ, phường 2, quận 1, HCM",
    geoLat: 10.776956,
    geoLong: 106.683288,
    totalRts: 32,
  },
  {
    id: 13,
    name: "Shop ABC",
    phone: "0989788748",
    address: "35 Nguyễn Văn Thủ, phường 2, quận 1, HCM",
    geoLat: 10.796956,
    geoLong: 106.683288,
    totalRts: 32,
  },
];

const DestinationHubComponent = ({
  courierInfo = {},
  processResponseApi,
  goToPickUpPage,
  goToSelectionHub,
  goToHandoverParcel,
  ...props
}) => {
  const [sellers, setSellers] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [showMap, setShowMap] = useState(false);

  const [selectedDestination, setSelectedDestionation] = useGlobalState(
    SELECTED_DESTINATION,
    {}
  );
  const [courierLocation, setCourierLocation] = useGlobalState(
    COURIER_LOCATION,
    []
  );

  const [showDialog, setShowDialog] = useState(false);

  const [geoLongDialog, setGeoLongDialog] = useState("");
  const [geoLatDialog, setGeoLatDialog] = useState("");

  useEffect(() => {
    changeCourierStatus(COURIER_FREE);
  }, []);

  const loadDataByLocation = useCallback(
    async (geoLat = 10.776956, geoLong = 106.683288) => {
      setIsLoading(true);

      try {
        const response = await apiPost("sellers/by-location", {
          geoLat,
          geoLong,
        });
        processResponseApi(response, (data) => {
          if (!data?.length) {
            data = mockData;
          }
          setSellers(data);
        });
      } catch {
        setSellers(mockData);
      }
      setIsLoading(false);
    },
    [processResponseApi]
  );

  const setListSellersForCourier = useCallback(async (sellerIds = []) => {
    setIsLoading(true);

    try {
      await apiPost("sellers/set-sellers-for-courier", {
        courierId: 1,
        sellerIds,
      });
    } catch {}
    setIsLoading(false);
  }, []);

  useEffect(() => {
    loadDataByLocation(...courierLocation);
    loadNodeList();
  }, [courierLocation, loadDataByLocation]);

  useEffect(() => {
    setGeoLatDialog("");
    setGeoLongDialog("");
  }, [showDialog]);

  const changeCourierStatus = useCallback(async (status = COURIER_PICKING) => {
    setIsLoading(true);
    try {
      await apiPost("couriers/change-status", {
        courierId: 1,
        status,
      });
    } catch {}
    setIsLoading(false);
  }, []);

  const loadNodeList = useCallback(async () => {
    try {
      const response = await apiGet("nodes/list");
      processResponseApi(response, (data) => {
        if (!courierInfo.destinationNodeId) return;

        const match = data.find((s) => s.id === courierInfo.destinationNodeId);
        setSelectedDestionation(match);
      });
    } catch {
      setIsLoading(false);
    }
  }, [
    processResponseApi,
    setSelectedDestionation,
    courierInfo.destinationNodeId,
  ]);

  return (
    <div>
      <Toolbar className="bean-toolbar">
        <BackButton
          style={{ transform: "translateY(7px)", visibility: "hidden" }}
          className="back-btn"
        />
        <div className="bean-title">
          <div className="select-destionation-page-header">
            <span
              className="ion-md-locate"
              onClick={() => {
                setShowDialog(true);
              }}
            ></span>
            Free Pickup
            {!showMap ? (
              <span
                className="ion-md-navigate"
                onClick={() => {
                  setShowMap(!showMap);
                }}
              ></span>
            ) : (
              <span
                className="ion-md-list"
                onClick={() => {
                  setShowMap(!showMap);
                }}
              ></span>
            )}
          </div>
        </div>
      </Toolbar>

      <Loading isLoading={isLoading} />

      <div className="destination-hub-container">
        <div className="top-part">
          <span>My Destination Hub</span>
          <span
            className="top-part-anyhub"
            onClick={() => {
              goToSelectionHub();
            }}
          >
            {selectedDestination?.name ? selectedDestination.name : "Any Hub"}
          </span>
        </div>

        {!showMap ? (
          <div className="destination-hub-body">
            {sellers.map((seller, index) => {
              return (
                <div
                  key={`destination-hub-item-${index}`}
                  className="hub-item-wrapper"
                >
                  <div className="hub-item-header">
                    <div>{seller.name || "-"}</div>
                    <div className="hub-item-header-group">
                      <Checkbox
                        onChange={(e) => {
                          const data = {
                            ...seller,
                            isSelected: e.target.checked,
                          };

                          const matchIndex = sellers.findIndex(
                            (s) => s.id === seller.id
                          );
                          if (matchIndex === -1) return;

                          const newSellers = [...sellers];
                          newSellers[matchIndex] = data;

                          setSellers(newSellers);
                        }}
                      />
                    </div>
                  </div>
                  <div className="hub-item-info">{seller?.address || "-"}</div>

                  <div className="hub-item-bottom seller-suggestion">
                    <div className="total-rts">
                      <img src={ImgBox} alt="box" />
                      <span className="nb"> {seller?.totalRts || "0"}</span>
                    </div>
                    <span className="distance"> {seller.distance} km </span>
                  </div>
                </div>
              );
            })}

            <div className="destination-hub-actions">
              <Button
                className="btn-large"
                disabled={
                  !sellers.filter((s) => s.isSelected).map((s) => s.id).length
                }
                onClick={async () => {
                  await setListSellersForCourier(
                    sellers.filter((s) => s.isSelected).map((s) => s.id)
                  );
                  await changeCourierStatus(COURIER_PICKING);
                  goToPickUpPage(sellers.filter((s) => s.isSelected))();
                }}
              >
                Start
              </Button>
              <Button
                className="btn-large"
                onClick={async () => {
                  goToHandoverParcel();
                }}
              >
                Full Capacity
              </Button>
            </div>
          </div>
        ) : (
          <FreePickupMapView
            sellers={sellers}
            goToPickUpPage={goToPickUpPage}
            goToHandoverParcel={goToHandoverParcel}
          />
        )}
      </div>

      {showDialog && (
        <Dialog
          style={{ height: "100%", width: "400" }}
          cancelable
          isOpen
          onCancel={() => setShowDialog(false)}
        >
          <div className="destination-hub-dialog-container">
            <div className="">Update Current Location</div>
            <Input
              className="input-box"
              placeholder="Longitude"
              value={geoLongDialog}
              onChange={(event) => {
                setGeoLongDialog(event.target.value);
              }}
            />
            <Input
              className="input-box"
              placeholder="Latitude"
              value={geoLatDialog}
              onChange={(event) => {
                setGeoLatDialog(event.target.value);
              }}
            />

            <Button
              onClick={() => {
                setCourierLocation([+geoLongDialog, +geoLatDialog]);
                setShowDialog(false);
              }}
            >
              Update
            </Button>
          </div>
        </Dialog>
      )}
    </div>
  );
};

export default DestinationHubComponent;
