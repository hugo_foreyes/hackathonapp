import React from "react";
import PageBase from "../components/PageBase";
import FreePickup from "./free-pickup";
import HandoverParcel from "./handover-parcel";
import ReturnHub from "./ReturnHub";
import Pickup from "./Pickup";
import { apiPost } from "../helper/apiConnector";
import { COURIER_FREE, COURIER_PICKING, COURIER_RETURNING } from "../constants";

class Home extends PageBase {
  constructor(props) {
    super(props);
    this.hasBackButton = false;
    this.isHaveToolbar = false;
    this.courierInfo = {};
  }

  renderToolbarAction() {
    return <span className="ion-md-create toolbar-button action-button"></span>;
  }

  async loadCourierInfo() {
    this.loading(true);
    try {
      let response = await apiPost("couriers/info-by-phone", {
        phone: "0989699332",
      });
      if (response.error) {
        this.loading(false);

        this.replacePage.bind(this, FreePickup, "free-pickup-page")();
        return;
      }
      this.processResponseApi(response, (data) => {
        this.setState({
          courierInfo: data,
        });
        this.loading(false);

        switch (data?.status) {
          case COURIER_FREE:
            this.replacePageWithProps.bind(
              this,
              FreePickup,
              "free-pickup-page",
              {
                courierInfo: this.state.courierInfo,
              }
            )();
            break;

          case COURIER_PICKING:
            this.replacePage.bind(this, Pickup, "pickup")();
            break;

          case COURIER_RETURNING:
            this.replacePage.bind(this, ReturnHub, "return-hub")();
            break;

          default:
            this.replacePage.bind(this, FreePickup, "free-pickup-page")();
        }
      });
    } catch {
      console.log("error here");
    }
  }

  componentDidMount() {
    this.loadCourierInfo();
  }

  renderContent() {
    return <div className="page-content"></div>;
  }
}

export default Home;
