import React from 'react';
import PageBase from '../components/PageBase';

class ChangeDestNode extends PageBase {

  constructor(props) {
    super(props);
    this.pageName = 'Change destination node';
  }

  renderContent() {
    return (
      <div className='page-content'>
        abc
      </div>
    );
  }
}

export default ChangeDestNode