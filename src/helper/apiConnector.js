// const apiHost = "http://localhost:8080/api/";
const apiHost = "http://hackathonserver.nhacuadau.tk/api/";

function processError(error, resolve) {
    console.log(error);
    resolve({
      "code": 500,
      "isSuccess": false,
      "error": {
        "message": "Hệ thống bị lỗi!"
      }
    });
}

function processResponse(response, resolve, url, requestOptions) {
    return resolve(response);
}

function generateHeader(headers) {
    if (null == headers || typeof headers === 'undefined') {
      headers = {};
    }
    return headers;
}

export async function apiGet(url) {
    return new Promise(resolve => {
        const requestOptions = {
          method: 'GET',
          headers: generateHeader()
        };
        fetch(apiHost + url, requestOptions)
              .then(res => {return res.json();})
              .then((response) => processResponse(response, resolve, url, requestOptions))
              .catch((error) => processError(error, resolve));
    });
}


export async function apiPut(url, data) {
    return new Promise(resolve => {
        const requestOptions = {
          method: 'PUT',
          headers: generateHeader({ 'Accept': 'application/json', 'Content-Type': 'application/json', "Access-Control-Allow-Origin": "*" }),
          body: JSON.stringify(data)
        };
        fetch(apiHost + url, requestOptions)
          .then(res => {return res.json();})
          .then((response) => processResponse(response, resolve, url, requestOptions))
          .catch((error) => processError(error, resolve));
    });
}


export async function apiPostFiles(url, data) {
    return new Promise(resolve => {
        const formData  = new FormData();
        for(const name in data) {
            formData.append(name, data[name]);
        }
        const requestOptions = {
          method: 'POST',
          headers: generateHeader({ "Access-Control-Allow-Origin": "*" }),
          body: formData
        };
        fetch(apiHost + url, requestOptions)
          .then(res => {return res.json();})
          .then((response) => processResponse(response, resolve, url, requestOptions))
          .catch((error) => processError(error, resolve));
    });
}

export async function apiPost(url, data) {
    return new Promise(resolve => {
        const requestOptions = {
          method: 'POST',
          headers: generateHeader({ 'Accept': 'application/json', 'Content-Type': 'application/json', "Access-Control-Allow-Origin": "*" }),
          body: JSON.stringify(data)
        };
        fetch(apiHost + url, requestOptions)
          .then(res => {return res.json();})
          .then((response) => processResponse(response, resolve, url, requestOptions))
          .catch((error) => processError(error, resolve));
    });
}

export async function apiDelete(url) {
    return new Promise(resolve => {
        const requestOptions = {method: 'DELETE', headers: generateHeader()}
        fetch(apiHost + url, requestOptions)
          .then(res => {return res.json();})
          .then((response) => processResponse(response, resolve, url, requestOptions))
          .catch((error) => processError(error, resolve));
    });
}

