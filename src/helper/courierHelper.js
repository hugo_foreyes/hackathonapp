export function getCourier() {
    const courier = JSON.parse(localStorage.getItem('courier'));
    if (typeof courier !== 'undefined' && courier != null) {
      return courier;
    }
    return null;
}

export function createCourier(data) {
  localStorage.setItem('courier', data);
}


export function deleteCourier(memberId) {
  localStorage.removeItem('courier');
}